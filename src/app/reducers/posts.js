import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

const getPosts = createAsyncThunk(
    'posts/getPosts',
    async ({start, limit}) => {

        return await fetch(`https://jsonplaceholder.typicode.com/posts?userId=7&_start=${start}&_limit=${limit}`).then(data => data.json());

    }
)
export const postsSlice = createSlice({
    name: "posts",
    initialState: {
        posts: [],
        loading: false
    },
    reducers: {},
    extraReducers: {
        [getPosts.pending]: state => {
            state.loading = true;
        },
        [getPosts.fulfilled]: (state, {payload}) => {
            state.loading = false;
            state.posts = [...state.posts, ...payload];
        },
        [getPosts.rejected]: (state) => {
            state.loading = false;
        }
    }
});

export default postsSlice.reducer;
export {getPosts};