import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";

const getUsers = createAsyncThunk(
    'users/getUsers',
    async (thunkAPI) => {
        const [res] = await fetch('https://jsonplaceholder.typicode.com/users?id=7').then(data => data.json());

        return res;
    }
);

export const usersSlice = createSlice({
    name: "users",
    initialState: {
        user: null,
        loading: false
    },
    reducers: {},
    extraReducers: {
        [getUsers.pending]: state => {
            state.loading = true;
        },
        [getUsers.fulfilled]: (state, { payload }) => {
            state.loading = false;
            state.user = payload;
        },
        [getUsers.rejected]: (state) => {
            state.loading = false;
        }
    }
});

export default usersSlice.reducer;
export {getUsers};