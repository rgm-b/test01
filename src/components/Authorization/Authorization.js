import styled from 'styled-components';
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getUsers} from "../../app/reducers/users";
import {useNavigate} from 'react-router-dom';

const Background = styled.div`
  background-color: #dfe6e8;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Container = styled.div`
  background-color: #fff;
  width: 534px;
  border-radius: 3px;
  text-align: center;
  margin: 0 10px;
`;

const Title = styled.h2`
  font-size: 21px;
  letter-spacing: 1px;
  font-weight: 600;
  margin: 17px 0;
`;

const Input = styled.input`
  outline: none;
  width: 88%;
  padding: 8px;
  font-size: 16px;
  margin-bottom: 16px;
  border: 2px solid #dcdcdc;
  border-radius: 2px;

  &:focus {
    border: 2px solid #9f9f9f;
  }
`;

const Button = styled.button`
  border: none;
  border-radius: 2px;
  width: 92%;
  padding: 14px 0;
  font-size: 16px;
  margin-bottom: 30px;
  color: #fff;
  cursor: pointer;
  transition: .2s;
  background-color: #bcefee;

  &:hover {
    background-color: #9fedec;
  }
`;


const userName = 'user';
const userPassword = '12345678';


function Authorization() {

    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [disabled, setDisabled] = useState(true);
    const navigate = useNavigate();
    const dispatch = useDispatch();


    const changeLogin = (event) => {
        setLogin(event.target.value);
    }

    const changePassword = (event) => {
        setPassword(event.target.value);
    }


    const buttonBackground = {
        background: disabled ? '#bcefee' : '#6ce7e6'
    }

    useEffect(() => {
        if (login === userName && password === userPassword) {
            setDisabled(false);
        } else {
            setDisabled(true);
        }
    }, [login, password]);


    useEffect(() => {
        dispatch(getUsers());
    }, [dispatch]);

    
    //получаем данные пользователя с сервера и по submit записываем в local storage
    const {user} = useSelector(state => state.users);
    
    const handleSubmit = (event) => {
        event.preventDefault();
        localStorage.setItem('person_id1', JSON.stringify(user));
        navigate("/");
    }


    return (
        <Background>
            <Container>
                <Title>
                    Вход
                </Title>
                <form onSubmit={handleSubmit}>
                    <Input onChange={changeLogin} value={login} placeholder="Логин"/>
                    <Input onChange={changePassword} value={password} placeholder="Пароль" type="password"/>
                    <Button disabled={disabled} style={buttonBackground}>Войти</Button>
                </form>
            </Container>
        </Background>
    );
}

export default Authorization;