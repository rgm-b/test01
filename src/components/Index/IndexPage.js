import Header from "./Header/Header";
import Posts from "./Posts/Posts";

function IndexPage(){
    return(
        <>
            <Header/>
            <Posts/>
        </>
    )
}

export default IndexPage;