import userSvg from './icons/user.svg';
import exitSvg from './icons/exit.svg';
import styled from 'styled-components';
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {getUsers} from "../../../app/reducers/users";
import {useNavigate} from "react-router-dom";


const PersonHeader = styled.header`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  max-width: 1420px;
  padding: 7px 0;
`;

const BoxImgPerson = styled.div`
  background-color: #dfe6e8;
  height: 35px;
  width: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 3px;
`;

const ImgPerson = styled.img`
  width: 22px;
`;

const NamePerson = styled.div`
  margin-left: 10px;
  letter-spacing: -.5px;
`;

const Button = styled.button`
  cursor: pointer;
  margin-left: 25px;
  display: flex;
  align-items: center;
`;

const ImgExit = styled.img`
  width: 23px;
`;

function Header() {

    const navigate = useNavigate();
    const dispatch = useDispatch();
    const {user, loading} = useSelector(state => state.users);

    useEffect(() => {
        dispatch(getUsers());
    }, [dispatch]);

    const unLogin = () => {
        localStorage.setItem('person_id1', '');
        navigate('/auth');
    }

    if (loading) {
        return <div>Loading...</div>;
    }

    return (
        <PersonHeader>
            <BoxImgPerson>
                <ImgPerson src={userSvg} alt="Иконка пользователя"/>
            </BoxImgPerson>
            {user && <NamePerson>{user.name}</NamePerson>}
            <Button onClick={unLogin}>
                <ImgExit src={exitSvg} alt="Выход"/>
            </Button>
        </PersonHeader>
    )
}

export default Header;