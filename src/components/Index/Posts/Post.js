import {useState, useEffect} from "react";
import {useParams} from "react-router-dom";
import styled from "styled-components";

const Container = styled.div`
  text-align: center;
`;

function Post(){
    const {id} = useParams();
    const [post, setPost] = useState('');

    useEffect(() => {
        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
            .then(res=>res.json())
            .then(data => setPost(data))
    }, [id])


    return(
        <Container>
            <div>Детальная информация о посте</div>
            <h1>{post.title}</h1>
            <p>{post.body}</p>
        </Container>
    )
}

export default Post;