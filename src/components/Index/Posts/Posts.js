import styled from 'styled-components';
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {getPosts} from "../../../app/reducers/posts";
import {Link} from "react-router-dom";


const PostsSection = styled.section`
  background-color: #dfe6e8;
  padding-bottom: 70px;
`;

const PostsContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  margin: 0 auto;
  max-width: 1140px;
  padding: 70px 0 0;
`;

const Post = styled.div`
  background-color: #fff;
  max-width: 350px;
  height: 300px;
  margin: 0 15px 29px;
  padding: 28px 20px;
  box-sizing: border-box;
  @media (max-width: 576px) {
    height: auto;
  }
`;

const Title = styled.h3`
  font-size: 22px;
  line-height: 25px;
  margin-bottom: 22px;
`;

const Description = styled.p`
  font-size: 16px;
  line-height: 25px;
`;

const ContainerCenter = styled.div`
  text-align: center;
`;

const Button = styled.button`
  padding: 10px 15px;
  cursor: pointer;
  border-radius: 3px;
  font-size: 18px;
`;


function Posts() {
    const {posts, loading} = useSelector(state => state.posts);

    console.log('posts', posts);

    const dispatch = useDispatch();

    const [start, setStart] = useState(0);
    const [limit, setLimit] = useState(5);
    const [show, setShow] = useState(true);
    const maxLimit = 10;

    useEffect(() => {

        if ( posts.length === maxLimit || posts.length === limit){
            return;
        }

        if (limit === maxLimit) {
            setShow(false)
        }

        dispatch(getPosts({start, limit}));

    }, [dispatch, limit, start]);


    const pagination = () => {
        if (limit !== maxLimit) {
            setStart(start + 5);
            setLimit(limit + 5);
        }
    }


    if (loading) {
        return <div>Loading...</div>;
    }

    return (
        <PostsSection>
            <PostsContainer>
                {
                    posts.map(post => {
                        return (
                            <Post key={post.id}>
                                <Title>{post.title}</Title>
                                <Description>{post.body}</Description>
                                <Link to={{
                                    pathname: `/posts/${post.id}`,
                                   /* propsSearch: `${post.title}`*/
                                }}>Читать далее</Link>
                            </Post>
                        );
                    })

                }
            </PostsContainer>
            <ContainerCenter>
                {show && <Button onClick={pagination}>Показать ещё</Button>}
            </ContainerCenter>
        </PostsSection>
    )
}

export default Posts;