import {Routes, Route, useNavigate} from "react-router-dom";
import Authorization from "./components/Authorization/Authorization";
import IndexPage from "./components/Index/IndexPage";
import {useEffect} from "react";
import Post from "./components/Index/Posts/Post";

let path = localStorage.getItem('person_id1');

function App() {

    const navigate = useNavigate();

    useEffect(() => {
        path ? navigate('/') : navigate('/auth')
    }, [path])

    return (
        <Routes>
            <Route path={'/auth'} element={<Authorization/>}/>
            <Route path={'/'} element={<IndexPage/>}/>
            <Route path={'/posts/:id'} element={<Post/>}/>
        </Routes>
    );
}

export default App;
